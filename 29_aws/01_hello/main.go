package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", index)
	http.ListenAndServe(":80", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Oh yeah, I'm running on AWS.")
}

// Check env
// go env

// Build
// GOOS=linux GOARCH=amd64 go build -o mybinary

// Copy binary "mybinary" to server
// scp -i /path/to/[your].pem mybinary ubuntu@[ec2-user-public-DNS]:

// SSH into your server

// Run your code
//   - sudo chmod 700 mybinary
//   - sudo ./mybinary
//   - check it in a browser at [public-IP]

// Exit
//   - ctrl + c
//   - exit
