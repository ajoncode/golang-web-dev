package main

import (
	"fmt"
	"net/http"

	uuid "github.com/satori/go.uuid"
)

func main() {
	http.HandleFunc("/", index)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func index(w http.ResponseWriter, req *http.Request) {
	cookie, err := req.Cookie("session")
	if err != nil {
		id, _ := uuid.NewV4()
		cookie := &http.Cookie{
			Name:  "session",
			Value: id.String(),
			// Secure: true, // use secure when using https
			HttpOnly: true,
			Path:     "/",
		}
		http.SetCookie(w, cookie)
	}

	fmt.Println(cookie)
}

// go run main.go
//
// session=beb1fbe0-d235-4b1d-a454-0c1b252bd84f

// browser -> http://localhost:8080/
// browser -> http://localhost:8080/
