package main

import (
	"log"
	"os"
	"text/template"
)

type item struct {
	Name, Descrip, Meal string
	Price               float64
}

type items []item

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	m := items{
		item{
			Name:    "Oatmeal",
			Descrip: "yum yum",
			Meal:    "Breakfast",
			Price:   4.95,
		},
		item{
			Name:    "Hamburger",
			Descrip: "Delicous good eating for you",
			Meal:    "Lunch",
			Price:   6.95,
		},
		item{
			Name:    "Pasta Bolognese",
			Descrip: "From Italy delicious eating",
			Meal:    "Dinner",
			Price:   7.95,
		},
	}
	err := tpl.Execute(os.Stdout, m)
	if err != nil {
		log.Fatalln(err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Document</title>
// </head>
// <body>
//     Oatmeal<br>
//     yum yum<br>
//     Breakfast 4.95<br>
//     Hamburger<br>
//     Delicous good eating for you<br>
//     Lunch 6.95<br>
//     Pasta Bolognese<br>
//     From Italy delicious eating<br>
//     Dinner 7.95<br>
// </body>
// </html>
