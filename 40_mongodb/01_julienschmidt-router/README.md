# Using Curl

1. Start your server
```
go run main.go
```

1. Enter this at the terminal
```
curl http://localhost:8080
```

This code forked from Steven White and his excellent articles:
https://stevenwhite.com/building-a-rest-service-with-golang-1/
https://stevenwhite.com/building-a-rest-service-with-golang-2/
https://stevenwhite.com/building-a-rest-service-with-golang-3/
