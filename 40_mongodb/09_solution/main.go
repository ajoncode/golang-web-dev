package main

import (
	"net/http"

	controllers "github.com/ajoncode/Golang-Web-Dev/40_mongodb/09_solution/controllers"
	models "github.com/ajoncode/Golang-Web-Dev/40_mongodb/09_solution/models"
	httprouter "github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	// Get a UserController instance
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:8080", r)
}

func getSession() map[string]models.User {
	return models.LoadUsers()
}

// go run main.go

// curl -X POST -H "Content-Type: application/json" -d '{"name":"Temari Nara","gender":"female","age":17}' http://localhost:8080/user
// {"id":"eaa00252-825e-4a0c-be00-177b0ac3f2cf","name":"Temari Nara","gender":"female","age":17}

// curl http://localhost:8080/user/eaa00252-825e-4a0c-be00-177b0ac3f2cf
// {"id":"eaa00252-825e-4a0c-be00-177b0ac3f2cf","name":"Temari Nara","gender":"female","age":17}

// curl -X DELETE http://localhost:8080/user/eaa00252-825e-4a0c-be00-177b0ac3f2cf
// Deleted user eaa00252-825e-4a0c-be00-177b0ac3f2cf
