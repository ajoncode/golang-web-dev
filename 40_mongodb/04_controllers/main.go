package main

import (
	"net/http"

	controllers "github.com/ajoncode/Golang-Web-Dev/40_mongodb/04_controllers/controllers"

	httprouter "github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController()
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe(":8080", r)
}

// go run *.go

// curl http://localhost:8080/user/1
// {"name":"Shikamaru Nara","gender":"male","age":17,"id":"1"}

// curl -X POST -H "Content-Type: application/json" -d '{"Name":"Temari Nara","Gender":"female","Age":21,"Id":"5"}' http://localhost:8080/user
// {"name":"Temari Nara","gender":"female","age":21,"id":"7"}

// curl -X DELETE -H "Content-Type: application/json" http://localhost:8080/user/7
// Write code to delete user
