package main

import (
	"net/http"

	controllers "github.com/ajoncode/Golang-Web-Dev/40_mongodb/07_solution/controllers"
	"github.com/ajoncode/Golang-Web-Dev/40_mongodb/07_solution/models"
	httprouter "github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	// Get a UserController instance
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe("localhost:8080", r)
}

// func getSession() *mgo.Session {
// 	// Connect to our local mongo
// 	s, err := mgo.Dial("mongodb://localhost")

// 	// Check if connection error, is mongo running?
// 	if err != nil {
// 		panic(err)
// 	}
// 	return s
// }

func getSession() map[string]models.User {
	// Connect to our local mongo
	// s, err := mgo.Dial("mongodb://localhost")

	// Check if connection error, is mongo running?
	// if err != nil {
	// 	panic(err)
	// }
	// return s

	return make(map[string]models.User)
}

// go run main.go

// curl -X POST -H "Content-Type: application/json" -d '{"name":"Temari Nara","gender":"female","age":17}' http://localhost:8080/user
// {"id":"661303d1-7698-46f0-9ea4-798296a4b570","name":"Temari Nara","gender":"female","age":17}

// curl http://localhost:8080/user/661303d1-7698-46f0-9ea4-798296a4b570
// {"id":"661303d1-7698-46f0-9ea4-798296a4b570","name":"Temari Nara","gender":"female","age":17}

// curl -X DELETE http://localhost:8080/user/661303d1-7698-46f0-9ea4-798296a4b570
// Deleted user661303d1-7698-46f0-9ea4-798296a4b570
