package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	models "github.com/ajoncode/Golang-Web-Dev/40_mongodb/07_solution/models"
	httprouter "github.com/julienschmidt/httprouter"
	uuid "github.com/satori/go.uuid"
)

type UserController struct {
	// session *mgo.Session
	session map[string]models.User
}

// func NewUserController(s *mgo.Session) *UserController {
// 	return &UserController{s}
// }
func NewUserController(m map[string]models.User) *UserController {
	return &UserController{m}
}

func (uc UserController) GetUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Grab id
	id := p.ByName("id")

	// Verify id is ObjectId hex representation, otherwise return status not found
	// if !bson.IsObjectIdHex(id) {
	// 	w.WriteHeader(http.StatusNotFound) // 404
	// 	return
	// }

	// ObjectIdHex returns an ObjectId from the provided hex representation.
	// oid := bson.ObjectIdHex(id)

	// composite literal
	// u := models.User{}

	// Fetch user
	// if err := uc.session.DB("go-web-dev-db").C("users").FindId(oid).One(&u); err != nil {
	// 	w.WriteHeader(404)
	// 	return
	// }

	// Retrieve user
	u := uc.session[id]

	// Marshal provided interface into JSON structure
	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK) // 200
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) CreateUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	u := models.User{}

	json.NewDecoder(r.Body).Decode(&u)

	// create bson ID
	// u.Id = bson.NewObjectId()

	// create ID
	uID, _ := uuid.NewV4()
	u.ID = uID.String()

	// store the user in mongodb
	// uc.session.DB("go-web-dev-db").C("users").Insert(u)

	// store the user
	uc.session[u.ID] = u

	uj, err := json.Marshal(u)
	if err != nil {
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated) // 201
	fmt.Fprintf(w, "%s\n", uj)
}

func (uc UserController) DeleteUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	// Grab id
	id := p.ByName("id")

	// if !bson.IsObjectIdHex(id) {
	// 	w.WriteHeader(404)
	// 	return
	// }

	// oid := bson.ObjectIdHex(id)

	// Delete user
	// if err := uc.session.DB("go-web-dev-db").C("users").RemoveId(oid); err != nil {
	// 	w.WriteHeader(404)
	// 	return
	// }

	// Delete User
	delete(uc.session, id)

	w.WriteHeader(http.StatusOK) // 200
	// fmt.Fprint(w, "Deleted user", oid, "\n")
	fmt.Fprint(w, "Deleted user", id, "\n")
}
