package main

import (
	"net/http"

	"gopkg.in/mgo.v2"

	controllers "github.com/ajoncode/Golang-Web-Dev/40_mongodb/05_mongodb/01_update-user-controller/controllers"
	httprouter "github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	// Get a UserController instance
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe(":8080", r)
}

func getSession() *mgo.Session {
	// Connect to our local mongo
	s, err := mgo.Dial("mongodb://localhost")
	// Check if connection error, is mongo running?
	if err != nil {
		panic(err)
	}
	return s
}

// go run *.go

// curl http://localhost:8080/user/1
// {"name":"Shikamaru Nara","gender":"male","age":17,"id":"1"}

// curl -X POST -H "Content-Type: application/json" -d '{"Name":"Shikamar Nara","Gender":"male","Age":17,"Id":"777"}' http://localhost:8080/user
// {"name":"Shikamar Nara","gender":"male","age":17,"id":"7"}
