package main

import (
	"net/http"

	mgo "gopkg.in/mgo.v2"

	controllers "github.com/ajoncode/Golang-Web-Dev/40_mongodb/05_mongodb/04_update-user-controllers-get/controllers"
	httprouter "github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe(":8080", r)
}

func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(err)
	}

	return s
}

// go run *.go
