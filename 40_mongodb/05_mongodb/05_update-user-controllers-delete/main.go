package main

import (
	"net/http"

	mgo "gopkg.in/mgo.v2"

	controllers "github.com/ajoncode/Golang-Web-Dev/40_mongodb/05_mongodb/05_update-user-controllers-delete/controllers"
	httprouter "github.com/julienschmidt/httprouter"
)

func main() {
	r := httprouter.New()
	uc := controllers.NewUserController(getSession())
	r.GET("/user/:id", uc.GetUser)
	r.POST("/user", uc.CreateUser)
	r.DELETE("/user/:id", uc.DeleteUser)
	http.ListenAndServe(":8080", r)
}

func getSession() *mgo.Session {
	s, err := mgo.Dial("mongodb://localhost")
	if err != nil {
		panic(err)
	}

	return s
}

// go run main.go

// curl -X POST -H "Content-Type: application/json" -d '{"name":"Temari Nara","gender":"female","age":17}' http://localhost:8080/user
// {"name":"Temari Nara","gender":"female","age":17,"id":"5fcde6d25a2d217304cb1aad"}

// curl http://localhost:8080/user/5fcde6d25a2d217304cb1aad
// {"name":"Temari Nara","gender":"female","age":17,"id":"5fcde6d25a2d217304cb1aad"}

// curl -X DELETE http://localhost:8080/user/5fcde6d25a2d217304cb1aad
// Deleted userObjectIdHex("5fcde6d25a2d217304cb1aad")
