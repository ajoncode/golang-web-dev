package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

type ninja struct {
	Name   string
	Quote  string
	Hokage bool
}

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {

	n1 := ninja{
		Name:   "Shikamaru Nara",
		Quote:  "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.",
		Hokage: false,
	}

	n2 := ninja{
		Name:   "Kakashi Hatake",
		Quote:  "I'm Hatake Kakashi. Things I Like And Things I Hate? I Don't Feel Like Telling You That. My Dreams For The Future? Never Really Thought About That. As For My Hobbies... I Have Lots Of Hobbies.",
		Hokage: true,
	}

	ninjas := []ninja{n1, n2}

	err := tpl.Execute(os.Stdout, ninjas)
	if err != nil {
		log.Fatalln("Error executing template: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>predefined global functions</title>
// </head>
// <body>
// EXAMPLE #1
//     {Shikamaru Nara Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me. false}
//     {Kakashi Hatake I'm Hatake Kakashi. Things I Like And Things I Hate? I Don't Feel Like Telling You That. My Dreams For The Future? Never Really Thought About That. As For My Hobbies... I Have Lots Of Hobbies. true}
// EXAMPLE #2
//     EXAMPLE #2 - [{Shikamaru Nara Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me. false} {Kakashi Hatake I'm Hatake Kakashi. Things I Like And Things I Hate? I Don't Feel Like Telling You That. My Dreams For The Future? Never Really Thought About That. As For My Hobbies... I Have Lots Of Hobbies. true}]
// EXAMPLE #3
//        EXAMPLE #3 - Shikamaru Nara
//        EXAMPLE #3 - Kakashi Hatake
// EXAMPLE #4
//         EXAMPLE #4 - Name: Kakashi Hatake
//         EXAMPLE #4 - Quote: I'm Hatake Kakashi. Things I Like And Things I Hate? I Don't Feel Like Telling You That. My Dreams For The Future? Never Really Thought About That. As For My Hobbies... I Have Lots Of Hobbies.
//         EXAMPLE #4 - Hokage: true
// </body>
// </html>
