package main

import (
	"log"
	"os"
	"text/template"
)

type character struct {
	Name string
	Team int
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	c1 := character{
		Name: "Shikamaru Nara",
		Team: 10,
	}

	err := tpl.Execute(os.Stdout, c1)
	if err != nil {
		log.Fatalln("Error executing template: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Composition</title>
// </head>
// <body>
// <h1>Shikamaru Nara</h1>
// <h2>10</h2>
// </body>
// </html>
