package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

type course struct {
	Number, Name, Units string
}

type semester struct {
	Term    string
	Courses []course
}

type year struct {
	Fall, Spring, Summer semester
}

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	y := year{
		Fall: semester{
			Term: "Fall",
			Courses: []course{
				course{"CSCI-40", "Introduction to Programming in Go", "4"},
				course{"CSCI-130", "Introduction to Web Programming with Go", "4"},
				course{"CSCI-140", "Mobile Apps Using Go", "4"},
			},
		},
		Spring: semester{
			Term: "Spring",
			Courses: []course{
				course{"CSCI-50", "Introduction to Programming in Rust - 1", "5"},
				course{"CSCI-190", "Introduction to Programming in Rust - 2", "5"},
				course{"CSCI-191", "Data Structures in RUST", "5"},
			},
		},
	}

	err := tpl.Execute(os.Stdout, y)
	if err != nil {
		log.Fatalln("Error executing template", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Document</title>
// </head>
// <body>
// Fall<br>
// CSCI-40 - Introduction to Programming in Go - 4<br>
// CSCI-130 - Introduction to Web Programming with Go - 4<br>
// CSCI-140 - Mobile Apps Using Go - 4<br>
// Spring<br>
// CSCI-50 - Introduction to Programming in Rust - 1 - 5<br>
// CSCI-190 - Introduction to Programming in Rust - 2 - 5<br>
// CSCI-191 - Data Structures in RUST - 5<br>
// </body>
// </html>
