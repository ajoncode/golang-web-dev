package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

type character struct {
	Name string
	Team int
}

type hokage struct {
	character
	Hokage bool
}

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	n1 := hokage{
		character{
			Name: "Shikamaru Nara",
			Team: 10,
		},
		false,
	}
	err := tpl.Execute(os.Stdout, n1)
	if err != nil {
		log.Fatalln("Error executing template: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Composition</title>
// </head>
// <body>
// <h1>Shikamaru Nara</h1>
// <h2>10</h2>
// <h3>Jonin</h3>
// </body>
// </html>
