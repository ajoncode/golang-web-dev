package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func main() {
	err := tpl.ExecuteTemplate(os.Stdout, "index.gohtml", "Kakash Hatake")
	if err != nil {
		log.Fatalln("Error executing template index.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Hello World!</title>
// </head>
// <body>
// <h1>I'm Hatake Kakashi. Things I Like And Things I Hate? I Don't Feel Like Telling You That. My Dreams For The Future? Never Really Thought About That. As For My Hobbies... I Have Lots Of Hobbies. - Kakash Hatake</h1>
// <p>
// Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me. - Shikamaru Nara
// </p>
// </body>
// </html>
