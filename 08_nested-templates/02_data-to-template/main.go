package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func main() {
	names := struct {
		First, Second string
	}{
		First:  "Kakashi Hatake",
		Second: "Shikamaru Nara",
	}
	err := tpl.ExecuteTemplate(os.Stdout, "index.gohtml", names)
	if err != nil {
		log.Fatalln("Error executing template index.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Hello World!</title>
// </head>
// <body>
// <h1>I'm Hatake Kakashi. Things I Like And Things I Hate? I Don't Feel Like Telling You That. My Dreams For The Future? Never Really Thought About That. As For My Hobbies... I Have Lots Of Hobbies.: Kakashi Hatake</h1>
// <p>
// Laziness is the mother of all bad habits, but ultimately she is a mother and we should respect her.
// - Shikamaru Nara
// </p>
// </body>
// </html>
