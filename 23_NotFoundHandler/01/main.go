package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	fmt.Println(req.URL.Path)
	fmt.Println(w, "go look at your terminal")
}

// go run main.go

// browser -> localhost:8080
// browser -> localhost:8080/favicon.ico
