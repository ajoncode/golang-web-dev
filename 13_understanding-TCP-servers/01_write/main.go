package main

import (
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln("Error listening on port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Panicln("Error accepting the connection: ", err)
			continue
		}

		io.WriteString(conn, "\n Hello from TCP server \n")
		fmt.Fprintln(conn, "How is your day?")
		fmt.Fprintf(conn, "%v", "Well I hope")

		conn.Close()
	}
}

/*
* go run main.go
* then go to another terminal window and enter this: telnet localhost 8080
 */

//  go run main.go

//  telnet localhost 8080
//  Trying 127.0.0.1...
//  Connected to localhost.
//  Escape character is '^]'.
//   Hello from TCP server
//  How is your day?
//  Well I hope
