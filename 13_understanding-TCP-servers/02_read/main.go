package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Panicln("Error listening to port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Println("Error accepting the connection: ", err)
			continue
		}
		go handle(conn)
	}
}

func handle(conn net.Conn) {
	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(ln)
	}
	defer conn.Close()

	// we never get here
	// we have an open stream connection
	// how does the above reader know when it's done?
	fmt.Println("Code got here.")
}

/*
* An open connection blocks.
* The reader is reading from the open connection.
* How does the reader know when it should stop reading?
* Instructions: run this file, then in your browser go to:
* (firefox, chrome call 2 request at a time)
* http://localhost:8080/
 */

//  go run main.go
//  GET / HTTP/1.1
//  Host: localhost:8080
//  User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:79.0) Gecko/20100101 Firefox/79.0
//  Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
//  Accept-Language: en-US,en;q=0.5
//  Accept-Encoding: gzip, deflate
//  Connection: keep-alive
//  Upgrade-Insecure-Requests: 1
