package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"time"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Println("Error listening on port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Fatalln("Error accepting connection: ", err)
			continue
		}
		go handle(conn)
	}
}

func handle(conn net.Conn) {
	err := conn.SetDeadline(time.Now().Add(10 * time.Second))
	if err != nil {
		log.Fatalln("Timeout error: ", err)
	}

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(ln)
		fmt.Fprintf(conn, "I heard you say: %s \n", ln)
	}
	defer conn.Close()

	// now we get here
	// the connection will time out
	// that breaks us out of the scanner loop
	fmt.Println("***CODE GOT HERE***")
}

/*
 * An open connection blocks.
 * The reader is reading from the open connection.
 * How does the reader know when it should stop reading?
 * Instructions: run this file, then open another terminal
 * and run telnet localhost 8080 and simply type words
 *
 * Connection will automatically be closed after 10 seconds
 */

// go run main.go
// hello
// ***CODE GOT HERE***

// telnet localhost 8080
// Trying 127.0.0.1...
// Connected to localhost.
// Escape character is '^]'.
// hello
// I heard you say: hello
// Connection closed by foreign host.
