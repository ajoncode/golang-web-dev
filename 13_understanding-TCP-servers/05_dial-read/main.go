package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", ":8080")
	if err != nil {
		log.Println("Error dialing port 8080: ", err)
	}
	defer conn.Close()

	bs, err := ioutil.ReadAll(conn)
	if err != nil {
		log.Fatalln("Error reading from connection: ", err)
	}
	fmt.Println(string(bs))
}

/*
 * run "01_write"
 * run "05_dial-read"
 */

/* 01_write  */
// go run main.go

/* 05_dial-read  */
//  go run main.go
//  Hello from TCP server
// How is your day?
// Well I hope
