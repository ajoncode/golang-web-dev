package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", ":8080")
	if err != nil {
		log.Panicln("Error dialing on port 8080: ", err)
	}
	defer conn.Close()

	fmt.Fprintln(conn, "I dialed you")
}

/*
 * run "02_read"
 * run "06_dial-write"
 */

/* 02_read */
// go run main.go
// I dialed you
// Code got here.

/* 06_dial_write */
// go run main.go
