package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Panicln("Error listening on port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Fatalln("Error accepting connection: ", err)
			continue
		}

		go handle(conn)
	}
}

func handle(conn net.Conn) {
	scanner := bufio.NewScanner(conn)

	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(ln)
		fmt.Fprintf(conn, "I heard you say: %s \n", ln)
	}
	defer conn.Close()

	// we never get here
	// we have an open stream connection
	// how does the above reader know when it's done?
	fmt.Println("Code got here.")
}

/*
 * An open connection blocks.
 * The reader is reading from the open connection.
 * How does the reader know when it should stop reading?
 * Instructions: run this file, then open another terminal
 * and run telnet localhost 8080 and simply type words
 */

// go run main.go
// hello
// how are you
// I am ajoncode

//  telnet localhost 8080
//  Trying 127.0.0.1...
//  Connected to localhost.
//  Escape character is '^]'.
//  hello
//  I heard you say: hello
//  how are you
//  I heard you say: how are you
//  I am ajoncode
//  I heard you say: I am ajoncode
