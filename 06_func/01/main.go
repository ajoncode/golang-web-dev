package main

import (
	"log"
	"os"
	"strings"
	"text/template"
)

var tpl *template.Template

type character struct {
	Name, Quote string
}

type team struct {
	Village, Jutsu string
	Team           int
}

// create a FuncMap to register functions.
// "uc" is a function
// "uc" is the ToUpper func from package strings
// "ft" is a function
// "ft" slices a string, returning the first three characters
var fm = template.FuncMap{
	"uc": strings.ToUpper,
	"ft": firstThree,
}

func firstThree(s string) string {
	s = strings.TrimSpace(s)
	if len(s) > 3 {
		s = s[:3]
	}
	return s
}

func init() {
	tpl = template.Must(template.New("").Funcs(fm).ParseFiles("tpl.gohtml"))
}

func main() {
	kakashi := character{
		Name:  "Kakashi Hatake",
		Quote: "I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.",
	}

	shikamaru := character{
		Name:  "Shikamaru Nara",
		Quote: "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.",
	}

	team7 := team{
		Village: "Konoha",
		Jutsu:   "Kamui",
		Team:    7,
	}

	team10 := team{
		Village: "Konoha",
		Jutsu:   "Shadow Possession Jutsu",
		Team:    10,
	}

	ninjas := []character{kakashi, shikamaru}
	teams := []team{team7, team10}

	konohaNinjas := struct {
		Characters []character
		Teams      []team
	}{ninjas, teams}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", konohaNinjas)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>functions</title>
// </head>
// <body>
// KAKASHI HATAKE
// I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.
// SHIKAMARU NARA
// Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.
// KONOHA
// 7
// Kam
// KONOHA
// 10
// Sha
// </body>
// </html>
