package main

import (
	"log"
	"os"
	"text/template"
	"time"
)

var tpl *template.Template

func monthDayYear(t time.Time) string {
	return t.Format("01-02-2006")
}

var fm = template.FuncMap{
	"fdateMDY": monthDayYear,
}

func init() {
	tpl = template.Must(template.New("").Funcs(fm).ParseFiles("tpl.gohtml"))
}

func main() {
	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", time.Now())
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

/**
 * The reference time used in the layouts is the specific time:
 * Mon Jan 2 15:04:05 MST 2006
 * which is Unix time 1136239445.
 *
 * Since MST is GMT-0700, the reference time can be thought of as
 * 01/02 03:04:05PM '06 -0700
 */

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>functions</title>
// </head>
// <body>
// Without date formatting: 2020-08-21 23:22:14.429815962 +0530 IST m=+0.000792578
// With date formatting: 08-21-2020
// </body>
// </html>
