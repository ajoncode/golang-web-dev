package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

type ninja string

func init() {
	tpl = template.Must(template.ParseFiles("index.gohtml"))
}

func (n ninja) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	err := req.ParseForm()
	if err != nil {
		log.Fatalln("Error parsing form: ", err)
	}

	tpl.ExecuteTemplate(w, "index.gohtml", req.Form)
}

func main() {
	var n ninja
	http.ListenAndServe(":8080", n)
}

/*
 * Form containes the parsed form data, including both the URL field's query
 * parameters and the POST or PUT form data.
 *
 * PostForm contains the parsed form data from POST, PATCH, or PUT body
 * parameters.
 *
 * To get Form or PostForm are only available after ParseForm is called.
 */

//  go run main.go
