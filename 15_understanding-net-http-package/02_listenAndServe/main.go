package main

import (
	"fmt"
	"net/http"
)

type ninja string

func (n ninja) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(w, "Any code we want in this function")
}

func main() {
	var n1 ninja
	http.ListenAndServe(":8080", n1)
}

// go run main.go

// browser -> localhost:8080
// Any code we want in this function
