package main

import (
	"fmt"
	"net/http"
)

type ninja string

func (n ninja) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("shikamaru-Key", "ShikamaruNara")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintln(w, "<h1>Any code you want in this func</h1>")
}

func main() {
	var n ninja
	http.ListenAndServe(":8080", n)
}

// go run main.go

// browser -> localhost:8080
// Any code you want in this func

// inspect -> network -> headers
// Content-Type: text/html; charset=utf-8
// Shikamaru-Key: ShikamaruNara
