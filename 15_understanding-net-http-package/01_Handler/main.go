package main

import (
	"fmt"
	"net/http"
)

type ninja string

/*
 * handler interface -> serveHttp(ResponseWriter, *Request)
 *
 * any value of type ninja implecits implements the handler interface,
 * and thus is also a handler
 */
func (n ninja) ServeHTTP(w http.ResponseWriter, req http.Request) {
	fmt.Println("Any code we want in this function")
}

func main() {

}
