package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", set)
	http.HandleFunc("/read", read)
	http.HandleFunc("/abundance", abundance)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func set(w http.ResponseWriter, req *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:  "my-cookie",
		Value: "Shikamaru Nara",
		Path:  "/",
	})

	fmt.Fprintln(w, "COOKIE WRITTEN - CHECK YOUR BROWSER")
	fmt.Fprintln(w, "in chrome go to: dev tools / application / cookies")
}

func read(w http.ResponseWriter, req *http.Request) {
	c1, err := req.Cookie("my-cookie")
	if err != nil {
		log.Println("Error fetching cookie my-cookie: ", err)
	} else {
		fmt.Fprintln(w, "Your Cookie #1: ", c1)
	}

	c2, err := req.Cookie("sensai-cookie")
	if err != nil {
		log.Println("Error fetching cookie sensai-cookie: ", err)
	} else {
		fmt.Fprintln(w, "Your Cookie #2: ", c2)
	}

	c3, err := req.Cookie("special-cookie")
	if err != nil {
		log.Println("Error fetching cookie special-cookie: ", err)
	} else {
		fmt.Fprintln(w, "Your Cookie #3: ", c3)
	}
}

func abundance(w http.ResponseWriter, req *http.Request) {
	http.SetCookie(w, &http.Cookie{
		Name:  "sensai-cookie",
		Value: "Kakashi Hatake",
	})

	http.SetCookie(w, &http.Cookie{
		Name:  "special-cookie",
		Value: "Jiraya",
	})

	fmt.Fprintln(w, "COOKIES WRITTEN - CHECK YOUR BROWSER")
	fmt.Fprintln(w, "in chrome go to: dev tools / application / cookies")
}

/*
 * In chrome go to: dev tools / application / cookies
 */

// go run main.go

// browser -> http://localhost:8080
// browser -> http://localhost:8080/read
// browser -> http://localhost:8080/abundance
// browser -> http://localhost:8080/read
