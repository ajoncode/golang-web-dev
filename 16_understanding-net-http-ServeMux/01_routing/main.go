package main

import (
	"io"
	"net/http"
)

type ninja string

func (n ninja) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/team10":
		io.WriteString(w, "Shikamaru Nara")
	case "/team7":
		io.WriteString(w, "Kakashi Hatake")
	}
}

func main() {
	var n ninja
	http.ListenAndServe(":8080", n)
}

// go run main.go

// browser -> http://localhost:8080/team10
// Shikamaru Nara

// browser -> http://localhost:8080/team7
// Kakashi Hatake
