package main

import (
	"io"
	"net/http"
)

type ninja string

func (n ninja) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Shikamaru Nara - Ninja - Team 10")
}

type samurai string

func (s samurai) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Samurai Jack - Samurai")
}

func main() {
	var n ninja
	var s samurai

	http.Handle("/ninja/", n)
	http.Handle("/samurai", s)

	// DefaultServeMux
	http.ListenAndServe(":8080", nil)
}

// go run main.go

// browser -> http://localhost:8080/ninja
// Shikamaru Nara - Ninja - Team 10

// browser -> http://localhost:8080/ninja/konoha
// Shikamaru Nara - Ninja - Team 10

// browser -> http://localhost:8080/samurai
// Samurai Jack - Samurai

// browser -> http://localhost:8080/samurai/jack
// 404 page not found
