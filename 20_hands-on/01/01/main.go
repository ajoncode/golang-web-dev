/*
 * ListenAndServe on port ":8080" using the default ServeMux.
 * Use HandleFunc to add the following routes to the default ServeMux:
 * "/" "/dog/" "/me/
 * Add a func for each of the routes.
 * Have the "/me/" route print out your name.
 */

package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", foo)
	http.HandleFunc("/dog/", bar)
	http.HandleFunc("/me/", myName)
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "foo ran")
}

func bar(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "bar ran")
}

func myName(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "hello Shikamaru")
}

// go run main.go

// browser -> http://localhost:8080
// foo ran

// browser -> http://localhost:8080/dog
// bar ran

// browser -> http://localhost:8080/me
// hello Shikamaru
