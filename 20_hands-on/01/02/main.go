/*
 * Take the previous program in the previous folder and change it so that:
 * a template is parsed and served
 * you pass data into the template
 */

package main

import (
	"html/template"
	"io"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("greeting.gohtml"))
}

func main() {
	http.HandleFunc("/", foo)
	http.HandleFunc("/dog/", bar)
	http.HandleFunc("/me/", ninja)
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "foo ran")
}

func bar(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "bar ran")
}

func ninja(w http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(w, "greeting.gohtml", "Shikamaru Nara")
	if err != nil {
		log.Fatalln("Error executing template greeting.gohtml: ", err)
	}
}

// go run main.go

// browser -> http://localhost:8080
// foo ran

// browser -> http://localhost:8080/dog
// bar ran

// browser -> http://localhost:8080/me
// Hello, Shikamaru Nara
