package main

import (
	"html/template"
	"io"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("greeting.gohtml"))
}

func main() {
	http.Handle("/", http.HandlerFunc(foo))
	http.Handle("/dog", http.HandlerFunc(bar))
	http.Handle("/me/", http.HandlerFunc(ninja))
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "foo ran")
}

func bar(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "bar ran")
}

func ninja(w http.ResponseWriter, req *http.Request) {
	err := tpl.ExecuteTemplate(w, "greeting.gohtml", "Shikamaru Nara")
	if err != nil {
		log.Fatalln("Error executing template greeting.gohtml: ", err)
	}
}

// go run main.go

// browser -> http://localhost:8080
// foo ran

// browser -> http://localhost:8080/dog
// bar ran

// browser -> http://localhost:8080/me
// Hello, Shikamaru Nara
