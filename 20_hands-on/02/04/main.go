package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln("Error listening on port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Fatalln("Error accepting connection: ", err)
			continue
		}

		go serve(conn)
	}
}

func serve(conn net.Conn) {
	defer conn.Close()

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		ln := scanner.Text()
		fmt.Println(ln)
		if ln == "" {
			// when ln is empty, header is done
			fmt.Println("THIS IS THE END OF THE HTTP REQUEST HEADERS")
			break
		}
	}
}

// go run main.go
// Shikamaru Nara
// Jiraya
//
// THIS IS THE END OF THE HTTP REQUEST HEADERS

// telnet localhost 8080
// Trying 127.0.0.1...
// Connected to localhost.
// Escape character is '^]'.
// Shikamaru Nara
// Jiraya
//
// Connection closed by foreign host.
