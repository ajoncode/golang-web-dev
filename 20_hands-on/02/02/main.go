package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln("Error listening on port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Fatalln("Error accepting connection: ", err)
			continue
		}
		defer conn.Close()

		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			ln := scanner.Text()
			fmt.Println(ln)
		}

		// we never get here
		// we have an open stream connection
		// how does the above reader know when it's done?
		fmt.Println("Code got here.")
		io.WriteString(conn, "I see you connected.")
	}
}

// go run main.go
// Shikamaru Nara
// Kakashi Hatake
// Jiraya

// telnet localhost 8080
// Trying 127.0.0.1...
// Connected to localhost.
// Escape character is '^]'.
// Shikamaru Nara
// Kakashi Hatake
// Jiraya
