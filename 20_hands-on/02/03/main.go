package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
)

func main() {
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalln("Error listening on port 8080: ", err)
	}
	defer li.Close()

	for {
		conn, err := li.Accept()
		if err != nil {
			log.Fatalln("Error accepting connection: ", err)
			continue
		}

		scanner := bufio.NewScanner(conn)
		for scanner.Scan() {
			ln := scanner.Text()
			fmt.Println(ln)
			if ln == "" {
				// when ln is empty, header is done
				fmt.Println("THIS IS THE END OF THE HTTP REQUEST HEADERS")
				break
			}
		}

		fmt.Println("Code got here.")
		io.WriteString(conn, "I see you connected.")

		conn.Close()
	}
}

// go run main.go
// Shikamaru Nara
// Kakashi Hatake
// Jiraya
//
// THIS IS THE END OF THE HTTP REQUEST HEADERS
// Code got here.

// telnet localhost 8080
// Trying 127.0.0.1...
// Connected to localhost.
// Escape character is '^]'.
// Shikamaru Nara
// Kakashi Hatake
// Jiraya
//
// I see you connected.Connection closed by foreign host.
