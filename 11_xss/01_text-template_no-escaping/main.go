package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

// Page is of exported type
type Page struct {
	Title, Heading, Input string
}

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	home := Page{
		Title:   "Nothing Escaped",
		Heading: "Nothing is escaped with text/template",
		Input:   `<script>alert("Yow!");</script>`,
	}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", home)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go > index.html

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Nothing Escaped</title>
// </head>
//     <body>
//         <h1>Nothing is escaped with text/template</h1>
//         <script>alert("Yow!");</script>
//     </body>
// </html>
