package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	tpl, err := template.ParseFiles("one.gohtml")
	if err != nil {
		log.Fatalln("Error while parsing template: ", err)
	}

	err = tpl.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Error while executing template one.gohtml: ", err)
	}

	/**
	* tpl, err = template.ParseFiles("two.gohtml", "vespa.gohtml")
	* 	this will create a tpl with 2 templates two.gohtml and vespa.gohtml
	*
	* tpl, err = tpl.ParseFiles("two.gohtml", "vespa.gohtml")
	*	this will add templates two.gohmtl and vespa.gohtml into tpl containing one.gohtml template
	 */
	tpl, err = tpl.ParseFiles("two.gohtml", "vespa.gohtml")
	if err != nil {
		log.Fatalln("Error while parsing templates one.gohtml and vespa.gohtml: ", err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "vespa.gohtml", nil)
	if err != nil {
		log.Fatalln("Error while executing template vespa.gohtml: ", err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "two.gohtml", nil)
	if err != nil {
		log.Fatalln("Error while executing template two.gohtml: ", err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "one.gohtml", nil)
	if err != nil {
		log.Fatalln("Error while executing template one.gohtml: ", err)
	}

	err = tpl.Execute(os.Stdout, nil)
	if err != nil {
		log.Fatalln("Error while executing template one.gohtml: ", err)
	}

}

// go run main.go
// ******
// ONE
// ************
// VESPA
// ************
// TWO
// ************
// ONE
// ************
// ONE
// ******
