package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	tpl, err := template.ParseFiles("tpl.gohtml")
	if err != nil {
		log.Fatalln("Error while parsing template tpl.gohtml: ", err)
	}

	nf, err := os.Create("index.html")
	if err != nil {
		log.Fatalln("Error while creating file index.html: ", err)
	}
	defer nf.Close()

	err = tpl.Execute(nf, nil)
	if err != nil {
		log.Fatalln("Error while executing template: ", err)
	}
}

// go run main.go
