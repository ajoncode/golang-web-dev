# Create an AMI (Amazon Machine Image)
1. EC2 / Instances / right-click instance / create image
  - image name: web-architecture-2019-10-31
  - description: web server 2019 October 31
  - no reboot: unchecked 
    - allowing your instance to reboot gives a better image
1. create image

## Launch a new instance of your AMI in a new availability zone (AZ)
1. what AZ is your current instance running in?
  - EC2 / instances / look at the availability zone and make note of it
1. launch a new instance from your AMI
  - EC2 / AMIs / right click / launch / next: configure
1. subnet: ```<choose a different AZ>``` / next: storage / next
1. tag
  - value: web-server-0002
1. security group
  - choose the "web-tier" security group we created
1. launch
  - specify "key pair" we want the instance to use
1. launch instance

## Add new EC2 instance to load balancer's target group
1. add the new instance to the target group
1. enter load balancer DNS into a browser to see your load balancer in action
  - refresh your browser to see the switching between web-servers-sg
