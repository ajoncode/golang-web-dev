package main

import "fmt"

func main() {
	name := "Shikamaru Nara"

	tpl := `
	<!DOCTYPE html>
	<html lang="en">
	<head>
	<meta charset="UTF-8">
	<title>Hello World!</title>
	</head>
	<body>
	<h1>` + name + `</h1>
	</body>
	</html>
	`

	fmt.Println(tpl)
}

/** 
* go run main.go > index.html
*	This will create an index.html file 
*/

// OUTPUT
// go run main.go
// 	<!DOCTYPE html>
// 	<html lang="en">
// 	<head>
// 	<meta charset="UTF-8">
// 	<title>Hello World!</title>
// 	</head>
// 	<body>
// 	<h1>Shikamaru Nara</h1>
// 	</body>
// 	</html>