package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func main() {
	name := os.Args[1]
	fmt.Println("os.Args[0]: ", os.Args[0])
	fmt.Println("os.Args[1]: ", os.Args[1])

	str := fmt.Sprint(`
		<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta charset="UTF-8">
		<title>Hello World!</title>
		</head>
		<body>
		<h1>` +
		name +
		`</h1>
		</body>
		</html>
	`)

	nf, err := os.Create("index.html")
	if err != nil {
		log.Fatalln("Error while creating file: ", err)
	}
	defer nf.Close()

	io.Copy(nf, strings.NewReader(str))
}

// go run main.go Shikamaru
// os.Args[0]:  /tmp/go-build235172354/b001/exe/main
// os.Args[1]:  Shikamaru
