package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", foo)
	http.HandleFunc("/bar", bar)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	ctx = context.WithValue(ctx, "userID", 7)
	ctx = context.WithValue(ctx, "name", "Shikamaru Nara")

	results := dbAccess(ctx)

	fmt.Fprintln(w, results)
}

func bar(w http.ResponseWriter, req *http.Request) {
	ctx := req.Context()

	log.Println(ctx)
	fmt.Fprintln(w, ctx)
}

func dbAccess(ctx context.Context) int {
	uid := ctx.Value("userID").(int)

	return uid
}

/*
 * per request variables
 * good candidate for putting into context
 */

// go run main.go

// browser -> http://localhost:8080/
// 7

// browser -> http://localhost:8080/bar
// context.Background.WithValue(type *http.contextKey, val <not Stringer>).WithValue(type *http.contextKey, val 127.0.0.1:8080).WithCancel.WithCancel
