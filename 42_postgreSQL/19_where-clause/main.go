package main

import (
	"database/sql" // 'database/sql' is using 'github.com/lib/pq' internally
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
)

var db *sql.DB

func init() {
	var err error

	// db, err := sql.Open("postgres", "postgres://userName:password@localhost/databaseName?sslmode=disable")
	db, err = sql.Open("postgres", "postgres://shikamaru:shikamaru@localhost/bookstore?sslmode=disable")
	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	fmt.Println("You connected to your database.")
}

// Book struct
type Book struct {
	isbn   string
	title  string
	author string
	price  float64
}

func main() {
	http.HandleFunc("/books", booksIndex)
	http.HandleFunc("/books/show", booksShow)
	http.ListenAndServe(":8080", nil)
}

func booksIndex(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}

	rows, err := db.Query("SELECT * FROM books;")
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	defer rows.Close()

	bks := make([]Book, 0)
	for rows.Next() {
		bk := Book{}
		err = rows.Scan(&bk.isbn, &bk.title, &bk.author, &bk.price) // order matters
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		bks = append(bks, bk)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	for _, bk := range bks {
		fmt.Fprintf(w, "%s, %s, %s, $%.2f\n", bk.isbn, bk.title, bk.author, bk.price)
	}
}

func booksShow(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}

	isbn := r.FormValue("isbn") // picks isbn value from url query
	if isbn == "" {
		http.Error(w, http.StatusText(400), http.StatusBadRequest)
		return
	}

	row := db.QueryRow("SELECT * FROM books WHERE isbn = $1", isbn)
	// $1 refers to the first argument i.e, in this case we are passing isbn into query in place of $1
	// and so on...

	bk := Book{}
	err := row.Scan(&bk.isbn, &bk.title, &bk.author, &bk.price)
	switch {
	case err == sql.ErrNoRows:
		http.NotFound(w, r)
		return
	case err != nil:
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}

	fmt.Fprintf(w, "%s, %s, %s, $%.2f\n", bk.isbn, bk.title, bk.author, bk.price)
}

// go run main.go
// You connected to your database.

// curl -i localhost:8080/books
// HTTP/1.1 200 OK
// Date: Mon, 21 Dec 2020 11:36:13 GMT
// Content-Length: 112
// Content-Type: text/plain; charset=utf-8

// 985-45123     , JS RULES, AJ, $9.45
// 923-45219     , GO RULES, AJ, $15.42
// 326-78524     , RUST RULES, AJ, $15.42

// curl -i localhost:8080/books/show?isbn=985-45123
// HTTP/1.1 200 OK
// Date: Mon, 21 Dec 2020 11:44:01 GMT
// Content-Length: 36
// Content-Type: text/plain; charset=utf-8

// 985-45123     , JS RULES, AJ, $9.45
