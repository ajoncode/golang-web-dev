package main

import (
	"database/sql" // 'database/sql' is using 'github.com/lib/pq' internally
	"fmt"

	_ "github.com/lib/pq"
)

func main() {
	// db, err := sql.Open("postgres", "postgres://userName:password@localhost/databaseName?sslmode=disable")
	db, err := sql.Open("postgres", "postgres://shikamaru:shikamaru@localhost/bookstore?sslmode=disable")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	err = db.Ping()
	if err != nil {
		panic(err)
	}

	fmt.Println("You connected to your database.")
}

// go run main.go
// You connected to your database.
