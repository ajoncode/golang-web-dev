package main

import (
	"database/sql" // 'database/sql' is using 'github.com/lib/pq' internally
	"fmt"

	_ "github.com/lib/pq"
)

// Book struct
type Book struct {
	isbn   string
	title  string
	author string
	price  float64
}

func main() {
	// db, err := sql.Open("postgres", "postgres://userName:password@localhost/databaseName?sslmode=disable")
	db, err := sql.Open("postgres", "postgres://shikamaru:shikamaru@localhost/bookstore?sslmode=disable")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	if err = db.Ping(); err != nil {
		panic(err)
	}

	fmt.Println("You connected to your database.")

	rows, err := db.Query("SELECT * FROM books;")
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	bks := make([]Book, 0)
	for rows.Next() {
		bk := Book{}
		err := rows.Scan(&bk.isbn, &bk.title, &bk.author, &bk.price) // order matters
		if err != nil {
			panic(err)
		}
		bks = append(bks, bk)
	}

	if err = rows.Err(); err != nil {
		panic(err)
	}

	for _, bk := range bks {
		// fmt.Println(bk.isbn, bk.title, bk.author, bk.price)
		fmt.Printf("%s, %s, %s, $%.2f\n", bk.isbn, bk.title, bk.author, bk.price)
	}
}

// go run main.go
// You connected to your database.
// 985-45123     , JS RULES, AJ, $9.45
// 923-45219     , GO RULES, AJ, $15.42
// 326-78524     , RUST RULES, AJ, $15.42
