package main

import (
	"database/sql" // 'database/sql' is using 'github.com/lib/pq' internally
	"fmt"
	"net/http"

	_ "github.com/lib/pq"
)

var db *sql.DB

func init() {
	var err error
	// db, err := sql.Open("postgres", "postgres://userName:password@localhost/databaseName?sslmode=disable")
	db, err = sql.Open("postgres", "postgres://shikamaru:shikamaru@localhost/bookstore?sslmode=disable")
	if err != nil {
		panic(err)
	}

	if err = db.Ping(); err != nil {
		panic(err)
	}

	fmt.Println("You connected to your database.")
}

// Book struct
type Book struct {
	isbn   string
	title  string
	author string
	price  float64
}

func main() {
	http.HandleFunc("/books", booksIndex)
	http.ListenAndServe(":8080", nil)
}

func booksIndex(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, http.StatusText(405), http.StatusMethodNotAllowed)
		return
	}

	rows, err := db.Query("SELECT * FROM books;")
	if err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}
	defer rows.Close()

	bks := make([]Book, 0)
	for rows.Next() {
		bk := Book{}
		err = rows.Scan(&bk.isbn, &bk.title, &bk.author, &bk.price) // order matters
		if err != nil {
			http.Error(w, http.StatusText(500), 500)
			return
		}
		bks = append(bks, bk)
	}

	if err = rows.Err(); err != nil {
		http.Error(w, http.StatusText(500), 500)
		return
	}

	for _, bk := range bks {
		fmt.Fprintf(w, "%s, %s, %s, $%.2f\n", bk.isbn, bk.title, bk.author, bk.price)
	}
}

// go run main.go
// You connected to your database.

// curl -i localhost:8080/books
// HTTP/1.1 200 OK
// Date: Sun, 20 Dec 2020 16:16:23 GMT
// Content-Length: 112
// Content-Type: text/plain; charset=utf-8

// 985-45123     , JS RULES, AJ, $9.45
// 923-45219     , GO RULES, AJ, $15.42
// 326-78524     , RUST RULES, AJ, $15.42
