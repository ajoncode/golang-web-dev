package main

import (
	"encoding/base64"
	"fmt"
)

func main() {
	s := "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me."

	s64 := base64.StdEncoding.EncodeToString([]byte(s))

	fmt.Println(len(s))
	fmt.Println(len(s64))
	fmt.Println(s)
	fmt.Println(s64)
}

// go run main.go
// 90
// 120
// Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.
// U29tZXRpbWVzIEkgd2lzaCBJIHdhcyBhIGNsb3VkLi4uIEp1c3QgZmxvYXRpbmcgYWxvbmcsIGdvaW5nIHdoZXJldmVyIHRoZSBicmVlemUgdGFrZXMgbWUu
