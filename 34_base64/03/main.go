package main

import (
	"encoding/base64"
	"fmt"
	"log"
)

func main() {
	s := "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me."

	s64 := base64.StdEncoding.EncodeToString([]byte(s))

	fmt.Println(s64)

	bs, err := base64.StdEncoding.DecodeString(s64)
	if err != nil {
		log.Fatalln("Error decoding the base64 string: ", err)
	}
	fmt.Println(string(bs))
}

// go run main.go
// U29tZXRpbWVzIEkgd2lzaCBJIHdhcyBhIGNsb3VkLi4uIEp1c3QgZmxvYXRpbmcgYWxvbmcsIGdvaW5nIHdoZXJldmVyIHRoZSBicmVlemUgdGFrZXMgbWUu
// Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.
