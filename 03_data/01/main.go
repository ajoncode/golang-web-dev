package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", "Shikamaru Nara")
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Hello World!</title>
// </head>
// <body>
// <h1>Laziness is the mother of all bad habits, but ultimately she is a mother and we should respect her: Shikamaru Nara</h1>
// </body>
// </html>
