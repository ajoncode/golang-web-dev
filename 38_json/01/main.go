package main

import (
	"encoding/json"
	"log"
	"net/http"
)

type person struct {
	Fname string
	Lname string
	Items []string
}

func main() {
	http.HandleFunc("/", foo)
	http.HandleFunc("/mshl", mshl)
	http.HandleFunc("/encd", encd)
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	s := `<!DOCTYPE html>
		<html lang="en">
		<head>
		<meta charset="UTF-8">
		<title>FOO</title>
		</head>
		<body>
		You are at foo
		</body>
		</html>`

	w.Write([]byte(s))
}

func mshl(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	p1 := person{
		Fname: "Shikamaru",
		Lname: "Nara",
		Items: []string{"Ninja", "Konoha Village", "Loves Temari Nara"},
	}

	j, err := json.Marshal(p1)
	if err != nil {
		log.Println(err)
	}

	w.Write(j)
}

func encd(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	p1 := person{
		Fname: "Shikamaru",
		Lname: "Nara",
		Items: []string{"Ninja", "Konoha Village", "Loves Temari Nara"},
	}

	err := json.NewEncoder(w).Encode(p1)
	if err != nil {
		log.Println(err)
	}
}

/*
 * Marshal (GO data to JSON) and Unmarshal (JSON to GO data) will store data in a varible
 * Encode (GO data to JSON) and Decode (JSON to GO data) will directly send the data to writer
 */

// go run main.go

// Browser -> http://localhost:8080/
// You are at foo

// Browser -> http://localhost:8080/mshl
// {"Fname":"Shikamaru","Lname":"Nara","Items":["Ninja","Konoha Village","Loves Temari Nara"]}

// Browser -> http://localhost:8080/encd
// {"Fname":"Shikamaru","Lname":"Nara","Items":["Ninja","Konoha Village","Loves Temari Nara"]}
