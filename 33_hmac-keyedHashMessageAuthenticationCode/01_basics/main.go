package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"io"
)

func main() {
	c := getCode("test@example.com")
	fmt.Println(c)
	c = getCode("test@exampl.com")
	fmt.Println(c)
}

func getCode(s string) string {
	h := hmac.New(sha256.New, []byte("oursecretkey"))
	io.WriteString(h, s)

	// %x provides hex encoding (%x renders the string in base-16, with two output characters per byte of input)
	return fmt.Sprintf("%X", h.Sum(nil))
}

// go run main.go
// 626C359FCCDC33A5C857786CB45B08AC5F241FF4AF85B965A5D75B2DA6FC2B2C
// 445A44C5547D1CC7E7F6217C2344796B934E12DBC9D57E8C5AB33D964BA753CF
