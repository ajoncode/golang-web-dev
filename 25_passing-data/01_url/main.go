package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	v := req.FormValue("name")
	fmt.Println(w, "Do my search: ", v)
}

// go run main.go

// browser -> http://localhost:8080?name=Shikamaru

// browser -> http://localhost:8080
