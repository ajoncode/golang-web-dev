package main

import (
	"html/template"
	"log"
	"net/http"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

type person struct {
	FirstName  string
	LastName   string
	Subscribed bool
}

func main() {
	http.HandleFunc("/", foo)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func foo(w http.ResponseWriter, req *http.Request) {
	// body
	bs := make([]byte, req.ContentLength)
	req.Body.Read(bs)
	body := string(bs)

	err := tpl.ExecuteTemplate(w, "index.gohtml", body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		log.Fatalln("Error executing template index.gohtml: ", err)
	}
}

// templates -> index.gohtml -> form -> enctype="multipart/form-data"

// go run main.go

// browser -> localhost:8080 -> fill form

// BODY: ------WebKitFormBoundaryShvtIvftYDlxYtwZ Content-Disposition: form-data; name="first" Shikamaru ------WebKitFormBoundaryShvtIvftYDlxYtwZ Content-Disposition: form-data; name="last" Nara ------WebKitFormBoundaryShvtIvftYDlxYtwZ Content-Disposition: form-data; name="subscribe" on ------WebKitFormBoundaryShvtIvftYDlxYtwZ--
