package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.Handle("/favicon.ico", http.NotFoundHandler())
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	v := req.FormValue("name")
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, `
		<form method="POST">
			<input type="text" name="name">
			<button type="submit" class="btn">SUBMIT</button>
		</form>
	<br>`+v)
}

// go run main.go

// browser -> http://localhost:8080
