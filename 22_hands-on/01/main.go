package main

import (
	"html/template"
	"io"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/", husky)
	http.HandleFunc("/dog/", dog)
	http.HandleFunc("/dog.jpg", chien)
	http.ListenAndServe(":8080", nil)
}

func husky(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "husky ran")
}

func dog(w http.ResponseWriter, req *http.Request) {
	tpl, err := template.ParseFiles("dog.gohtml")
	if err != nil {
		log.Fatalln("Error parsing template dog.gohtml: ", err)
	}
	tpl.ExecuteTemplate(w, "dog.gohtml", nil)
}

func chien(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "./dog.jpg")
}

// go run main.go

// browser -> http://localhost:8080/

// browser -> http://localhost:8080/dog

// browser -> http://localhost:8080/dog.jpg
