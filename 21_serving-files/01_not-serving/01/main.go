package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")

	io.WriteString(w, `
	<!--not serving from our server. Server from a link-->
	<img src="https://images-na.ssl-images-amazon.com/images/I/81bjvJTcbWL._SL1500_.jpg">
	`)
}

// go run main.go

// browser -> http://localhost:8080/
