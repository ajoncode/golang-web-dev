package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, `
	<!--image doesn't serve-->
	<img src="/shikamaru.png">
	`)
}

// go run main.go
// browser -> http://localhost:8080/
