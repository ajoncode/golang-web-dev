package main

import (
	"log"
	"net/http"
)

func main() {
	// http.ListenAndServe(":8080", http.FileServer(http.Dir(".")))
	log.Fatalln(http.ListenAndServe(":8080", http.FileServer(http.Dir("."))))
}

// go run main.go

// browser -> http://localhost:8080
