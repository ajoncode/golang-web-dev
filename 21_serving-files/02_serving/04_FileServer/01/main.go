package main

import (
	"io"
	"net/http"
)

func main() {
	http.Handle("/", http.FileServer(http.Dir(".")))
	http.HandleFunc("/shikamaru/", ninja)
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, `
	<!--image served-->
	<img src="/shikamaru.png">
	`)
}

// go run main.go

// browser -> http://localhost:8080
// main.go
// shikamaru.png

// browser -> http://localhost:8080/shikamaru
