package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.Handle("/assets/", http.StripPrefix("/assets", http.FileServer(http.Dir("./assets"))))
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, `
	<!--image served-->
	<img src="/assets/shikamaru.png">
	`)
}

// go run main.go

// browser -> http://localhost:8080/

// browser -> http://localhost:8080/assets
