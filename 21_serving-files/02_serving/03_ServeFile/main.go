package main

import (
	"io"
	"net/http"
)

func main() {
	http.HandleFunc("/", ninja)
	http.HandleFunc("/shikamaru", ninjaImg)
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, `
	<!--image doesn't serve-->
	<img src="/shikamaru.png">
	`)
}

func ninjaImg(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "shikamaru.png")
}

// go run main.go

// browser -> http://localhost:8080

// browser -> http://localhost:8080/shikamaru
