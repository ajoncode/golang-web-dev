package main

import (
	"io"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", ninja)
	http.HandleFunc("/shikamaru", ninjaImg)
	http.ListenAndServe(":8080", nil)
}

func ninja(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	io.WriteString(w, `
	<!--image doesn't serve-->
	<img src="/shikamaru.png">
	`)
}

func ninjaImg(w http.ResponseWriter, req *http.Request) {
	f, err := os.Open("shikamaru.png")
	if err != nil {
		http.Error(w, "File not found", 404)
		return
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		http.Error(w, "File not found", 404)
		return
	}

	http.ServeContent(w, req, f.Name(), fi.ModTime(), f)
}

// go run main.go

// browser -> http://localhost:8080

// browser -> http://localhost:8080/shikamaru
