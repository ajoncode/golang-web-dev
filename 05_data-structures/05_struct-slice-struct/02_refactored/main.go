package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

type character struct {
	Name, Quote string
}

type team struct {
	Village, Jutsu string
	Team           int
}

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	kakashi := character{
		Name:  "Kakashi Hatake",
		Quote: "I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.",
	}

	shikamaru := character{
		Name:  "Shikamaru Nara",
		Quote: "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.",
	}

	team7 := team{
		Village: "Konoha",
		Jutsu:   "Kamui",
		Team:    7,
	}

	team10 := team{
		Village: "Konoha",
		Jutsu:   "Shadow Possession Jutsu",
		Team:    10,
	}

	ninjas := []character{kakashi, shikamaru}
	teams := []team{team7, team10}

	konohaNinjas := struct {
		Characters []character
		Teams      []team
	}{ninjas, teams}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", konohaNinjas)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>Characters & Team</title>
// </head>
// <body>
// <ul>
//     <li>I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies. - Kakashi Hatake</li>
//     <li>Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me. - Shikamaru Nara</li>
// </ul>
// <ul>
//     <li>Konoha - 7 - Kamui</li>
//     <li>Konoha - 10 - Shadow Possession Jutsu</li>
// </ul>
// </body>
// </html>
