package main

import (
	"log"
	"os"
	"text/template"
)

type character struct {
	Name  string
	Quote string
}

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	kakashi := character{
		Name:  "Kakashi Hatake",
		Quote: "I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.",
	}
	shikamaru := character{
		Name:  "Shikamaru Nara",
		Quote: "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.",
	}
	characters := []character{
		kakashi,
		shikamaru,
	}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", characters)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>My Peeps</title>
// </head>
// <body>
// <ul>
//     <li>I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies. - Kakashi Hatake</li>
//     <li>Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me. - Shikamaru Nara</li>
// </ul>
// </body>
// </html>
