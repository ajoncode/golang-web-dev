package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	characters := []string{"Kakashi Hatake", "Shikamaru Nara", "Temari Nara", "Gara"}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", characters)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>My Peeps</title>
// </head>
// <body>
// <ul>
//     <li>0 - Kakashi Hatake</li>
//     <li>1 - Shikamaru Nara</li>
//     <li>2 - Temari Nara</li>
//     <li>3 - Gara</li>
// </ul>
// </body>
// </html>
