package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	characters := []string{"Kakashi Hatake", "Shikamaru Nara", "Temari Nara", "Gara"}

	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", characters)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>My Peeps</title>
// </head>
// <body>
// <ul>
//     <li>Kakashi Hatake</li>
//     <li>Shikamaru Nara</li>
//     <li>Temari Nara</li>
//     <li>Gara</li>
// </ul>
// </body>
// </html>
