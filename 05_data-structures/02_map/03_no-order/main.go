package main

import (
	"fmt"
)

func main() {

	characters := map[string]string{
		"Jiraya":    "The true measure of a shinobi is not how he lives, but how he dies.",
		"Shikamaru": "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.",
		"Kakashi":   "I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.",
		"Temari":    "Let's see if you can do this without CRYING this time.",
	}

	for k, v := range characters {
		fmt.Println(k, " - ", v)
	}

}

// no distinct order

// go run main.go
// Kakashi  -  I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.
// Temari  -  Let's see if you can do this without CRYING this time.
// Jiraya  -  The true measure of a shinobi is not how he lives, but how he dies.
// Shikamaru  -  Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.
