package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("tpl.gohtml"))
}

func main() {
	characters := map[string]string{
		"Jiraya":    "The true measure of a shinobi is not how he lives, but how he dies.",
		"Shikamaru": "Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me.",
		"Kakashi":   "I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies.",
		"Temari":    "Let's see if you can do this without CRYING this time.",
	}
	err := tpl.ExecuteTemplate(os.Stdout, "tpl.gohtml", characters)
	if err != nil {
		log.Fatalln("Error executing template tpl.gohtml: ", err)
	}
}

// go run main.go
// <!DOCTYPE html>
// <html lang="en">
// <head>
//     <meta charset="UTF-8">
//     <title>My Peeps</title>
// </head>
// <body>
// <ul>
//     <li>The true measure of a shinobi is not how he lives, but how he dies. - Jiraya</li>
//     <li>I'm Hatake Kakashi. Things I like and things I hate? I don't feel like telling you that. My dreams for the future? Never really thought about that. As for my hobbies... I have lots of hobbies. - Kakashi</li>
//     <li>Sometimes I wish I was a cloud... Just floating along, going wherever the breeze takes me. - Shikamaru</li>
//     <li>Let's see if you can do this without CRYING this time. - Temari</li>
// </ul>
// </body>
// </html>
