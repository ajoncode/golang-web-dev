package main

import (
	"io"
	"net/http"
)

func ninja(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Shikamaru Nara - Ninja - Team 10")
}

func samurai(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Samurai Jack - Samurai")
}

func main() {
	http.Handle("/ninja/", http.HandlerFunc(ninja))
	http.Handle("/samurai", http.HandlerFunc(samurai))

	// DefaultServeMux
	http.ListenAndServe(":8080", nil)
}

/*
 * this is similar to this:
 * https://play.golang.org/p/X2dlgVSIrd
 * ---and this---
 * https://play.golang.org/p/YaUYR63b7L
 */

// go run main.go

// browser -> http://localhost:8080/ninja
// Shikamaru Nara - Ninja - Team 10

// browser -> http://localhost:8080/ninja/konoha
// Shikamaru Nara - Ninja - Team 10

// browser -> http://localhost:8080/samurai
// Samurai Jack - Samurai

// browser -> http://localhost:8080/samurai/jack
// 404 page not found
